# nodejs 基础

## [Nodejs 介绍](http://nodejs.cn/learn)

Node.js 是一个基于 Chrome V8 引擎的 JavaScript 运行时。

## Nodejs 可以做什么

- 轻量级、高性能的 web 服务
- 前后端 JavaScript 同构开发
- 便捷高效的前端工程化

## Nodejs 架构

![note](./imgs/1.png)

### Native modules

它是由 js 写成，提供我们应用程序调用的库，同时这些模块又依赖 builtin modules 来获取相应的服务支持

- 当前层内容由 JS 实现
- 提供应用程序可直接调用的库，例如 fs、path、http 等
- JS 语言无法直接操作底层硬件设备

### Builtin modules

它是由 C++代码写成各类模块，包含了 crypto，zlib, file stream etc 基础功能。（v8 提供了函数接口，libuv 提供异步 IO 模型库，以及一些 nodejs 函数，为 builtin modules 提供服务）。

### V8

主要有两个作用

1. 虚拟机的功能，执行 js 代码（自己的代码，第三方的代码和 native modules 的代码）。

2. 提供 C++函数接口，为 nodejs 提供 v8 初始化，创建 context，scope 等。

### libuv

![note](./imgs/3.png)

它是基于事件驱动的异步 IO 模型库，我们的 js 代码发出请求，最终由 libuv 完成，而我们所设置的回调函数则是在 libuv 触发。

## Nodejs 异步 I/O

在 NodeJS 中利用单线程，远离死锁、状态同步问题，利用异步 I/O，让单线程远离阻塞，以便更好的使用 CPU。异步 I/O 是期望 I/O 的调用不再阻塞后续运算，将原有等待 I/O 完成这段时间分配给其他需要的业务去执行。

- 阻塞 I/O
  - 重复调用 I/O 操作，判断 I/O 是否结束；
  - 例如：read、select、poll、kqueue、event ports。
- 非阻塞 I/O 是在调用后立即返回。

![note](./imgs/2.png)

- 同步：执行任务时，后面的任务需要等到前一个任务执行结束才可以执行，任务执行的顺序和任务排列的顺序是相同的
- 异步：同时处理多个任务，通过回调触发，任务执行的顺序和任务排列的顺序是不一定相同的

![note](./imgs/4.png)

### 异步 I/O 总结

- I/O 是应用程序的瓶颈所在（消耗硬件性能）
- 异步 I/O 提高性能无需原地等待结果返回（提高 CPU 利用率）
- I/O 操作属于操作系统级别，平台都有对应实现
- Nodejs 单线程配合事件驱动架构及 libuv 实现了异步 I/O

## Nodejs 事件驱动架构

事件驱动架构是软件开发过程中的通用模式

### EventEmitter 模块

EventEmitter 是促进 Node 中对象之间交流的模块，它是 Node 异步事件驱动机制的核心。Node 中很多自带的模块都继承自事件触发模块。

概念很简单：触发器触发事件，该事件对应的监听函数被调用。也就是说，触发器有两个特征：

- 触发某个事件
- 注册／注销监听函数

```javascript
const EventEmitter = require('events');

const myEvent = new EventEmitter();

// 类似订阅者
myEvent.on('event1', () => {
  console.log('事件1执行了');
});

myEvent.on('event1', () => {
  console.log('事件1-2执行了');
});

// 类似发布者
myEvent.emit('event1');
```

## Nodejs 单线程

使用 JS 实现高效可伸缩的高性能 Web 服务

### Nodejs 如何实现高并发
