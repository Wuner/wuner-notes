# 角色管理

## 功能

- 按条件分页查询角色列表
- 添加/编辑/删除角色
- 分配菜单
- 分配资源

## 角色添加、编辑、展示

### 新增数据类型

`src/types/index.d.ts`

```ts
export interface RoleFilter {
  code?: string; // 编号
  id?: number; // 资源ID
  name?: string; // 资源名称
  startCreateTime?: string; // 开始创建时间
  endCreateTime?: string; // 结束创建时间
  current?: number; // 当前页码
  size?: number; // 一页数量
}

export interface Role {
  code: string; // 角色编码
  id: number | string; // 主键ID
  name: string; // 角色名称
  createdBy: string; // 创建人
  description: string; // 角色描述
  updatedBy: string; // 更新人
  createdTime: string; // 创建时间
  updatedTime: string; // 更新时间
  operatorId: number; // 操作人ID
}
```

### 新增接口 API

`src/services/role/index.ts`

```ts
import request, { get } from '@/utils/request';
import { Role, RoleFilter } from '@/types';

/**
 * 保存或者更新角色
 * @param data
 */
export const saveOrUpdate = (data: Role) => {
  return request({
    url: '/boss/role/saveOrUpdate',
    method: 'POST',
    data,
  });
};

/**
 * 按条件查询角色
 * @param data
 */
export const getRolePages = (data: RoleFilter) => {
  return request({
    url: '/boss/role/getRolePages',
    method: 'POST',
    data,
  });
};

/**
 * 删除角色
 */
export const delRole = (id: string | number) => {
  return request({
    url: `/boss/role/${id}`,
    method: 'DELETE',
  });
};
```

### 页面

```vue
<template>
  <div class="role">
    <el-form
      :inline="true"
      ref="form"
      :model="roleFilter"
      class="demo-form-inline"
    >
      <el-form-item label="角色名称" prop="name">
        <el-input v-model="roleFilter.name" placeholder="角色名称"></el-input>
      </el-form-item>
      <el-form-item label="角色编号" prop="code">
        <el-input v-model="roleFilter.code" placeholder="角色编号"></el-input>
      </el-form-item>
      <el-form-item label="注册时间" prop="dateRange">
        <el-date-picker
          v-model="roleFilter.dateRange"
          @change="onDateChange"
          type="daterange"
          align="right"
          unlink-panels
          range-separator="至"
          start-placeholder="开始日期"
          end-placeholder="结束日期"
          :picker-options="pickerOptions"
        >
        </el-date-picker>
      </el-form-item>
      <el-form-item>
        <el-button type="primary" @click="onSubmit">查询</el-button>
        <el-button @click="resetForm">重置</el-button>
      </el-form-item>
    </el-form>
    <el-button
      style="margin-bottom: 20px"
      type="primary"
      icon="el-icon-plus"
      @click="show = true"
    >
      添加角色
    </el-button>
    <el-table v-loading="loading" :data="records" style="width: 100%">
      <el-table-column
        align="center"
        label="编号"
        prop="code"
        min-width="180"
      />
      <el-table-column
        align="center"
        label="角色名称"
        prop="name"
        min-width="180"
      />
      <el-table-column
        align="center"
        label="描述"
        prop="description"
        min-width="180"
      />
      <el-table-column
        align="center"
        label="添加时间"
        prop="createdTime"
        min-width="180"
      >
        <template slot-scope="scope">
          {{ scope.row.createdTime | date }}
        </template>
      </el-table-column>
      <el-table-column align="center" label="操作" min-width="180">
        <template slot-scope="scope">
          <el-button size="mini" @click="handleAssigningMenus(scope.row)">
            分配菜单
          </el-button>
          <el-button size="mini" @click="handleAssigningResources(scope.row)">
            分配资源
          </el-button>
          <el-button
            style="margin-top: 10px"
            size="mini"
            @click="handleEdit(scope.row)"
          >
            编辑
          </el-button>
          <el-button
            style="margin-top: 10px"
            size="mini"
            type="danger"
            @click="handleDelete(scope.row)"
          >
            删除
          </el-button>
        </template>
      </el-table-column>
    </el-table>
    <el-pagination
      v-loading="loading"
      style="margin-top: 20px"
      @size-change="handleSizeChange"
      @current-change="handleCurrentChange"
      :current-page="roleFilter.current"
      :page-sizes="[10, 20, 30, 40]"
      :page-size="roleFilter.size"
      layout="total, sizes, prev, pager, next, jumper"
      :total="total"
    >
    </el-pagination>
    <el-dialog
      :title="isEdit ? '编辑角色' : '添加角色'"
      :visible.sync="show"
      @close="onCancel"
    >
      <el-form ref="roleForm" :rules="rules" :model="roleForm">
        <el-form-item label="角色名称" prop="name">
          <el-input v-model="roleForm.name" autocomplete="off"></el-input>
        </el-form-item>
        <el-form-item label="角色编号" prop="code">
          <el-input v-model="roleForm.code" autocomplete="off"></el-input>
        </el-form-item>
        <el-form-item label="描述" prop="description">
          <el-input
            v-model="roleForm.description"
            type="textarea"
            autocomplete="off"
          ></el-input>
        </el-form-item>
      </el-form>
      <div slot="footer" class="dialog-footer">
        <el-button @click="onCancel">取 消</el-button>
        <el-button type="primary" @click="onSaveOrUpdateSubmit"
          >确 定</el-button
        >
      </div>
    </el-dialog>
  </div>
</template>

<script lang="ts">
import Vue from 'vue';
import { DatePicker, Form } from 'element-ui';
import { delRole, getRolePages, saveOrUpdate } from '@/services/role';
import { Role } from '@/types';

export default Vue.extend({
  name: 'LoginIndex',
  data() {
    return {
      loading: false,
      show: false,
      isEdit: false,
      roleForm: {
        name: '',
        code: '',
        description: '',
      } as Role,
      rules: {
        name: {
          required: true,
          message: '请输入角色名称',
          trigger: 'blur',
        },
        code: [
          {
            required: true,
            message: '请输入角色编号',
            trigger: 'blur',
          },
        ],
      },
      total: 0,
      roleFilter: {
        name: '',
        dateRange: '',
        code: '',
        startCreateTime: '',
        endCreateTime: '',
        current: 1,
        size: 10,
      },
      pickerOptions: {
        shortcuts: [
          {
            text: '最近一周',
            onClick(picker: DatePicker) {
              const end = new Date();
              const start = new Date();
              start.setTime(start.getTime() - 3600 * 1000 * 24 * 7);
              picker.$emit('pick', [start, end]);
            },
          },
          {
            text: '最近一个月',
            onClick(picker: DatePicker) {
              const end = new Date();
              const start = new Date();
              start.setTime(start.getTime() - 3600 * 1000 * 24 * 30);
              picker.$emit('pick', [start, end]);
            },
          },
          {
            text: '最近三个月',
            onClick(picker: DatePicker) {
              const end = new Date();
              const start = new Date();
              start.setTime(start.getTime() - 3600 * 1000 * 24 * 90);
              picker.$emit('pick', [start, end]);
            },
          },
        ],
      },
      records: [],
    };
  },
  methods: {
    reload() {
      this.resetForm();
      this.onSubmit();
    },
    onCancel() {
      (this.$refs.roleForm as Form).resetFields();
      this.roleForm = {} as Role;
      this.show = false;
    },
    async onSaveOrUpdateSubmit() {
      try {
        await saveOrUpdate(this.roleForm);
        this.$message.success('添加成功');
        this.onCancel();
        this.reload();
      } catch (e) {
        this.$message.error(e);
      }
    },
    onSubmit() {
      this.roleFilter.current = 1;
      this.loadRolePages();
    },
    resetForm() {
      this.roleFilter.startCreateTime = '';
      this.roleFilter.endCreateTime = '';
      (this.$refs.form as Form).resetFields();
    },
    onDateChange(val: string[]) {
      this.roleFilter.startCreateTime = val[0];
      this.roleFilter.endCreateTime = val[1];
    },
    async loadRolePages() {
      try {
        this.loading = true;
        const { records, total } = await getRolePages(this.roleFilter);
        this.loading = false;
        this.total = total;
        this.records = records;
        console.log(records);
      } catch (e) {
        this.loading = false;
        this.$message.error(e);
      }
    },
    handleAssigningMenus(item: Role) {
      this.$router.push({
        name: 'roleAssigningMenus',
        params: {
          id: item.id + '',
        },
      });
    },
    handleAssigningResources(item: Role) {
      this.$router.push({
        name: 'roleAssigningResource',
        params: {
          id: item.id + '',
        },
      });
    },
    handleEdit(item: Role) {
      this.isEdit = true;
      this.show = true;
      this.roleForm = JSON.parse(JSON.stringify(item));
    },
    handleDelete(item: Role) {
      this.$confirm('是否删除角色？').then(async () => {
        try {
          await delRole(item.id);
          this.reload();
          this.$message.success('删除成功');
        } catch (e) {
          this.$message.error(e);
        }
      });
    },
    handleSizeChange(size: number) {
      this.roleFilter.current = 1;
      this.roleFilter.size = size;
      this.loadRolePages();
    },
    handleCurrentChange(current: number) {
      this.roleFilter.current = current;
      this.loadRolePages();
    },
  },
  created() {
    this.loadRolePages();
  },
});
</script>
```

## 分配菜单

### 新增接口 API

`src/services/menu/index.ts`

```ts
/**
 * 获取角色拥有的菜单列表
 */
export const getRoleMenus = (params: object) => {
  return get('/boss/menu/getRoleMenus', params);
};

/**
 * 给角色分配菜单
 */
export const allocateRoleMenus = (data: object) => {
  return request({
    url: '/boss/menu/allocateRoleMenus',
    method: 'POST',
    data,
  });
};
```

### 页面

`src/views/role/assigning-menus.vue`

```vue
<template>
  <div class="assigning-menus">
    <el-card class="box-card">
      <el-tree
        ref="tree"
        :data="data"
        show-checkbox
        node-key="id"
        default-expand-all
        :default-checked-keys="selected"
        :props="defaultProps"
      >
      </el-tree>
      <div class="assigning-menus-button">
        <el-button type="primary" @click="onSave">保存</el-button>
        <el-button @click="onClear">清空</el-button>
      </div>
    </el-card>
  </div>
</template>

<script lang="ts">
import Vue from 'vue';
import { allocateRoleMenus, getRoleMenus } from '@/services/menu';
import { Menu } from '@/types';
import { Tree } from 'element-ui';

export default Vue.extend({
  name: 'assigning-menus',
  data() {
    return {
      data: [],
      defaultProps: {
        children: 'subMenuList',
        label: 'name',
      },
      selected: [] as number[],
    };
  },
  methods: {
    getSelectedMenus(menus: Menu[]) {
      menus.forEach((menu: Menu) => {
        if (menu.selected) {
          this.selected = [...this.selected, menu.id || -1];
        } else {
          this.selected = this.selected.filter(
            (value) => menu.parentId !== value,
          );
        }
        if (menu.subMenuList && menu.subMenuList.length > 0) {
          this.getSelectedMenus(menu.subMenuList);
        }
      });
    },
    onClear() {
      (this.$refs.tree as Tree).setCheckedNodes([]);
    },
    onSave() {
      this.$confirm('是否确认分配菜单？').then(async () => {
        try {
          await allocateRoleMenus({
            roleId: this.$route.params.id,
            menuIdList: (this.$refs.tree as Tree).getCheckedKeys(),
          });
          this.$message.success('分配菜单成功');
          this.$router.go(-1);
        } catch (e) {
          this.$message.error(e);
        }
      });
    },
  },
  async created() {
    try {
      const data = await getRoleMenus({ roleId: this.$route.params.id });
      this.data = data;
      this.getSelectedMenus(data);
      console.log(data);
    } catch (e) {
      this.$message.error(e);
    }
  },
});
</script>

<style lang="scss" scoped>
.assigning-menus {
  &-button {
    margin-top: 20px;
    display: flex;
    justify-content: center;
  }
}
</style>
```

## 分配资源

### 新增接口 API

`src/services/resource/index.ts`

```ts
/**
 * 获取角色拥有的资源列表
 * @param roleId 角色ID
 */
export const getRoleResources = (roleId: number | string) => {
  return get('/boss/resource/getRoleResources', { roleId });
};

/**
 * 给角色分配资源
 */
export const allocateRoleResources = (data: object) => {
  return request({
    url: `/boss/resource/allocateRoleResources`,
    method: 'POST',
    data,
  });
};
```

### 页面

`src/views/role/assigning-resource.vue`

```vue
<template>
  <div class="assigning-resource">
    <el-card class="box-card">
      <el-card
        style="margin-top: 10px"
        v-for="(item, index) in data"
        :key="index"
      >
        <div slot="header" class="clearfix">
          <el-checkbox
            :indeterminate="item.indeterminate"
            v-model="item.checkAll"
            @change="handleCheckAllChange(item)"
          >
            {{ item.name }}
          </el-checkbox>
        </div>
        <el-checkbox-group v-model="item.checkedList">
          <el-checkbox
            v-for="resource in item.resourceList"
            :key="resource.id"
            :label="resource.id"
            @change="handleCheckedCitiesChange(item)"
          >
            {{ resource.name }}
          </el-checkbox>
        </el-checkbox-group>
      </el-card>
      <div class="assigning-resource-button">
        <el-button type="primary" @click="onSave">保存</el-button>
        <el-button @click="onClear">清空</el-button>
      </div>
    </el-card>
  </div>
</template>

<script lang="ts">
import Vue from 'vue';
import { getRoleResources, allocateRoleResources } from '@/services/resource';
import { Resource } from '@/types';

export default Vue.extend({
  name: 'AssigningResource',
  data() {
    return {
      data: [] as Resource[],
      defaultProps: {
        children: 'resourceList',
        label: 'name',
      },
      selected: [] as number[],
    };
  },
  methods: {
    onClear() {
      this.data.forEach((value) => {
        value.checkedList = [];
        value.checkAll = false;
        value.indeterminate = false;
      });
    },
    onSave() {
      this.$confirm('是否确认分配资源？').then(async () => {
        try {
          let resourceIdList = [] as number[];
          this.data.forEach((value) => {
            resourceIdList = resourceIdList.concat(value.checkedList || []);
          });
          await allocateRoleResources({
            roleId: this.$route.params.id,
            resourceIdList,
          });
          this.$message.success('分配资源成功');
          this.$router.go(-1);
        } catch (e) {
          this.$message.error(e);
        }
      });
    },
    getSelectedResources(resources: Resource[]) {
      resources.forEach((resource: Resource) => {
        resource.checkedList = [];
        if (resource.selected) {
          if (resource.resourceList && resource.resourceList.length > 0) {
            resource.resourceList.forEach(
              (value) =>
                value.selected && resource.checkedList!.push(value.id || -1),
            );
            resource.indeterminate =
              resource.resourceList.length !== resource.checkedList.length;
            resource.checkAll =
              resource.resourceList.length === resource.checkedList.length;
          } else {
            resource.checkAll = true;
            resource.indeterminate = true;
          }
        } else {
          resource.checkAll = false;
          resource.indeterminate = false;
        }
      });
      return resources;
    },
    handleCheckAllChange(resource: Resource) {
      resource.indeterminate = false;
      if (resource.checkAll) {
        resource.resourceList &&
          resource.resourceList.forEach((value) =>
            resource.checkedList!.push(value.id || -1),
          );
      } else {
        resource.checkedList = [];
      }
    },
    handleCheckedCitiesChange(resource: Resource) {
      resource.indeterminate =
        resource.checkedList!.length > 0 &&
        resource.resourceList!.length !== resource.checkedList!.length;
      resource.checkAll =
        resource.resourceList!.length === resource.checkedList!.length;
      console.log(resource);
    },
  },
  async created() {
    try {
      const data = await getRoleResources(this.$route.params.id);
      (data as object[]).forEach((val: any) => {
        val.checkAll = false;
        val.checkedCities = [];
      });
      this.data = this.getSelectedResources(data);
      console.log(this.data);
    } catch (e) {
      this.$message.error(e);
    }
  },
});
</script>

<style lang="scss" scoped>
.assigning-resource {
  &-button {
    margin-top: 20px;
    display: flex;
    justify-content: center;
  }
}
</style>
```
