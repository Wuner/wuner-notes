# Vue.js + Vuex + TypeScript 实战项目开发与项目优化

本模块中我们通过使用 TypeScript 编程语言，基于 Vue.js 全家桶（Vue.js、Vue Router、Vuex、Element UI）开发 B 端管理系统项目（dashboard)。通过实战深入掌握 Vue.js 及其相关技术栈的使用。

## [项目源码](https://gitee.com/Wuner/edu-boss-wuner)
