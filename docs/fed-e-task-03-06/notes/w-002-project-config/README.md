# 项目配置

## 代码规范和 [ESLint](../../../../fed-e-task-02-02/notes/w-005-lint/w-001-eslint/)

不管是多⼈合作还是个⼈项⽬，代码规范都是很重要的。这样做不仅可以很⼤程度地避免基本语法错误， 也保证了代码的可读性。

## 导⼊ [Element](https://element.eleme.cn/#/zh-CN) 组件库

Element，⼀套为开发者、设计师和产品经理准备的基于 Vue 2.0 的桌⾯端组件库。

### 安装

#### npm

```shell
npm i element-ui
```

#### yarn

```shell
yarn add element-ui
```

### [快速上手](https://element.eleme.cn/#/zh-CN/component/quickstart)

## 样式处理

### ⽬录结构

```markdown
src/styles
├── index.scss # 全局样式（在⼊⼝模块被加载⽣效）
├── mixin.scss # 公共的 mixin 混⼊（可以把重复的样式封装为 mixin 混⼊到复⽤ 的地⽅）
├── reset.scss # 重置基础样式
└── variables.scss # 公共样式变量
```

### variables.scss

```scss
$primary-color: #40586f;
$success-color: #51cf66;
$warning-color: #fcc419;
$danger-color: #ff6b6b;
$info-color: #868e96; // #22b8cf;

$body-bg: #e9eef3; // #f5f5f9;

$sidebar-bg: #f8f9fb;
$navbar-bg: #f8f9fb;

$font-family: system-ui, -apple-system, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif;
```

### index.scss

```scss
@import './variables.scss';

// globals
html {
  font-family: $font-family;
  -webkit-text-size-adjust: 100%;
  -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
  // better Font Rendering
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
}

body {
  margin: 0;
  background-color: $body-bg;
}

// custom element theme
$--color-primary: $primary-color;
$--color-success: $success-color;
$--color-warning: $warning-color;
$--color-danger: $danger-color;
$--color-info: $info-color;
// change font path, required
$--font-path: '~element-ui/lib/theme-chalk/fonts';
// import element default theme
@import '~element-ui/packages/theme-chalk/src/index';
// node_modules/element-ui/packages/theme-chalk/src/common/var.scss

// overrides

// .el-menu-item, .el-submenu__title {
//   height: 50px;
//   line-height: 50px;
// }

.el-pagination {
  color: #868e96;
}

// components

.status {
  display: inline-block;
  cursor: pointer;
  width: 0.875rem;
  height: 0.875rem;
  vertical-align: middle;
  border-radius: 50%;

  &-primary {
    background: $--color-primary;
  }

  &-success {
    background: $--color-success;
  }

  &-warning {
    background: $--color-warning;
  }

  &-danger {
    background: $--color-danger;
  }

  &-info {
    background: $--color-info;
  }
}
```

### [共享全局样式变量](https://cli.vuejs.org/zh/guide/css.html#%E5%90%91%E9%A2%84%E5%A4%84%E7%90%86%E5%99%A8-loader-%E4%BC%A0%E9%80%92%E9%80%89%E9%A1%B9)

```js
// vue.config.js
module.exports = {
  css: {
    loaderOptions: {
      // 默认情况下 `sass` 选项会同时对 `sass` 和 `scss` 语法同时生效
      // 因为 `scss` 语法在内部也是由 sass-loader 处理的
      // 但是在配置 `prependData` 选项的时候
      // `scss` 语法会要求语句结尾必须有分号，`sass` 则要求必须没有分号
      // 在这种情况下，我们可以使用 `scss` 选项，对 `scss` 语法进行单独配置
      scss: {
        prependData: `@import "~@/styles/variables.scss";`,
      },
    },
  },
};
```

## 接口配置

数据接⼝，分别是：

1. `http://eduboss.lagou.com`
   - [接口文档](http://eduboss.lagou.com/boss/doc.html#/home)
2. `http://edufront.lagou.com`
   - [接口文档](http://113.31.105.128/front/doc.html#/home)

### 跨域配置

简单总结⼀下⼏种跨域解决⽅案。

我最推荐的也是我⼯作中在使⽤的⽅式就是： `cors` 全称为 `Cross Origin Resource Sharing`（跨域资 源共享）。这种⽅案对于前端来说没有什么⼯作量，和正常发送请求写法上没有任何区别，⼯作量基本都 在后端这⾥。每⼀次请求，浏览器必须先以 `OPTIONS` 请求⽅式发送⼀个预请求（也不是所有请求都会发 送 `options`，展开介绍 [点我](https://panjiachen.github.io/awesome-bookmarks/blog/cs.html#cors) ），通过预检请求从⽽获知服务器端对跨源请求⽀持的 `HTTP` ⽅法。在确认 服务器允许该跨源请求的情况下，再以实际的 `HTTP` 请求⽅法发送那个真正的请求。推荐的原因是：只要 第⼀次配好了，之后不管有多少接⼝和项⽬复⽤就可以了，⼀劳永逸的解决了跨域问题，⽽且不管是开发 环境还是正式环境都能⽅便的使⽤。详细 [MDN ⽂档](https://developer.mozilla.org/zh-CN/docs/Web/HTTP/Access_control_CORS)

但总有后端觉得麻烦不想这么搞，那纯前端也是有解决⽅案的。

在 `dev` 开发模式下可以下使⽤ `webpack` 的 `proxy` 使⽤也是很⽅便，参照 [⽂档](https://www.webpackjs.com/configuration/dev-server/#devserver-proxy) 就会使⽤了。但这种⽅法在⽣产环境是不能使⽤的。在⽣产环境中需要使⽤ `nginx` 进⾏反向代理。不管是 `proxy` 和 `nginx` 的原理都是⼀样的，通过搭建⼀个中转服务器来转发请求规避跨域的问题。

| 开发环境 | ⽣产环境 |
| -------- | -------- |
| cors     | cors     |
| proxy    | nginx    |

这⾥我只推荐这两种⽅式跨域，其它的跨域⽅式都还有很多但都不推荐，真⼼主流的也就这两种⽅式。

配置客户端层⾯的服务端代理跨域可以参考官⽅⽂档中的说明：

- [vue-cli](https://cli.vuejs.org/zh/config/#devserver-proxy)
- [http-proxy-middleware](https://github.com/chimurai/http-proxy-middleware)

vue.config.js

```js
module.exports = {
  devServer: {
    proxy: {
      '/boss': {
        target: 'http://eduboss.lagou.com',
        changeOrigin: true, // 把请求头中的 host 配置为 target
      },
      '/front': {
        target: 'http://edufront.lagou.com',
        changeOrigin: true,
      },
    },
  },
};
```

### 封装请求模块

#### 安装

npm

```shell
npm i axios
```

yarn

```shell
yarn add axios
```

#### 创建 src/utils/request.js

```ts
import axios from 'axios';

const request = axios.create({
  // 配置选项
  // baseURL,
  // timeout
});

// 请求拦截器

// 响应拦截器

export default request;
```

## 配置环境变量

- [配置 Vue 项⽬中的环境变量](https://cli.vuejs.org/zh/guide/mode-and-env.html#%E6%A8%A1%E5%BC%8F)
- [dotenv](https://github.com/motdotla/dotenv)
