# Token 处理

## Token 过期处理

调用接口时，接口返回的 http 的状态码 为 401 时，代表 token 无效，此时我们将在响应拦截器中，处理 401 状态码。

- 如果用户登陆信息不存在，直接跳转登陆页
- 否则调用 token 刷新接口，获取用户登陆信息
  - 如果接口调用失败，则跳转登陆页
  - 接口调用成功时，将用户登陆信息更新，并将上一次的请求，重新发送

```ts
import axios from 'axios';
import qs from 'qs';
import store from '@/store';
import { Store } from 'vuex';
import { RootState } from '@/types';
import { Message } from 'element-ui';
import router from '@/router';

const request = axios.create({
  timeout: 10000,
});

/**
 * 刷新 token
 * @param data
 */
const refreshToken = (data: object) => {
  return axios.create()({
    url: '/front/user/refresh_token',
    method: 'POST',
    data: qs.stringify(data),
    headers: { 'content-type': 'application/x-www-form-urlencoded' },
  });
};

/**
 * 跳转登陆页
 */
const redirectToLogin = () => {
  store.commit('user/setUserLoginInfo', null);
  router.push({
    path: '/login',
    query: {
      redirect: router.currentRoute.fullPath,
    },
  });
};

// 添加请求拦截器
request.interceptors.request.use(
  function (config) {
    // 在发送请求之前做些什么
    const { access_token: accessToken } =
      (store as Store<RootState>).getters['user/userLoginInfo'] || {};
    if (accessToken) {
      config.headers.Authorization = accessToken;
    }
    return config;
  },
  function (error) {
    // 对请求错误做些什么
    return Promise.reject(error);
  },
);

// 添加响应拦截器
request.interceptors.response.use(
  function (response) {
    // 对响应数据做点什么
    const { data } = response;
    const { state, content, message } = data;
    if (state === 200 || state === 1) {
      if (typeof content === 'string') return JSON.parse(content);
      else return content;
    } else {
      return Promise.reject(message);
    }
  },
  async function (error) {
    // 对响应错误做点什么
    if (error.response) {
      // 请求成功并收到响应，但是状态码超出了 2xx 到范围
      const { status, config } = error.response;
      if (status === 401) {
        const userLoginInfo = store.getters['user/userLoginInfo'];
        // 如果用户登陆信息不存在，直接跳转登陆页
        if (userLoginInfo) {
          const { refresh_token: refreshtoken } = userLoginInfo;
          try {
            // 调用token刷新接口，获取用户登陆信息
            const { data } = await refreshToken({ refreshtoken });
            store.commit('user/setUserLoginInfo', JSON.parse(data.content));
            // 将上一次的请求，重新发送
            return request(config);
          } catch (e) {
            redirectToLogin();
          }
        } else {
          redirectToLogin();
        }
      } else if (status === 404) {
        Message.error('请求资源不存在');
      } else {
        Message.error('接口请求失败，请联系管理员');
      }
    } else if (error.request) {
      // 请求成功并未收到响应
      Message.error('请求超时');
      console.log(error.request);
    } else {
      // 设置请求时触发的某些错误
      Message.error('请求错误');
      console.log('Error', error.message);
    }
    return Promise.reject(error);
  },
);

const post = (url: string, data: object) => {
  return request({
    url,
    method: 'POST',
    data: qs.stringify(data),
    headers: { 'content-type': 'application/x-www-form-urlencoded' },
  });
};

const get = (url: string, params: object) => {
  return request({
    url,
    method: 'GET',
    params,
    headers: { 'content-type': 'application/x-www-form-urlencoded' },
  });
};

export { post, get };
export default request;
```

## 关于多次请求的问题

当连续调用多个接口时，上面的处理则会有问题。

刷新 token 接口使用的参数是一次性的，不能用于多次请求，并且 token 刷新接口调用失败时，其返回的响应状态码是 200，所以上面的 try catch 的处理是有问题的，将会导致后续接口调用依然是 401，将导致其跳转到登陆页面。

- 设置 isRefreshToken
  —是否处于刷新 token 中
- 设置 requests
  —处于刷新 token 时的接口请求数组
- 在刷新状态下时，将请求的接口挂起到 requests 数组中
- 调用 token 刷新接口，获取用户登陆信息
  - 将状态置为 token 刷新中
  - 接口调用成功
    - 更新用户登录信息
    - 将 requests 数组中挂起的请求，重新发送出去
    - 将 requests 数组重置
    - 将上一次的请求，重新发送
  - 接口调用失败，跳转至登录页
  - 接口请求结束将状态重置

```ts
import axios from 'axios';
import qs from 'qs';
import store from '@/store';
import { Store } from 'vuex';
import { RootState } from '@/types';
import { Message } from 'element-ui';
import router from '@/router';

// 是否处于刷新token中
let isRefreshToken = false;
// 处于刷新token时的接口请求数组
let requests: Function[] = [];

const request = axios.create({
  timeout: 10000,
});

/**
 * 刷新 token
 * @param data
 */
const refreshToken = (data: object) => {
  return axios.create()({
    url: '/front/user/refresh_token',
    method: 'POST',
    data: qs.stringify(data),
    headers: { 'content-type': 'application/x-www-form-urlencoded' },
  });
};

/**
 * 跳转登陆页
 */
const redirectToLogin = () => {
  store.commit('user/setUserLoginInfo', null);
  router.push({
    path: '/login',
    query: {
      redirect: router.currentRoute.fullPath,
    },
  });
};

// 添加请求拦截器
request.interceptors.request.use(
  function (config) {
    // 在发送请求之前做些什么
    const { access_token: accessToken } =
      (store as Store<RootState>).getters['user/userLoginInfo'] || {};
    if (accessToken) {
      config.headers.Authorization = accessToken;
    }
    return config;
  },
  function (error) {
    // 对请求错误做些什么
    return Promise.reject(error);
  },
);

// 添加响应拦截器
request.interceptors.response.use(
  function (response) {
    // 对响应数据做点什么
    const { data } = response;
    const { state, content, message } = data;
    if (state === 200 || state === 1) {
      if (typeof content === 'string') return JSON.parse(content);
      else return content;
    } else {
      return Promise.reject(message);
    }
  },
  async function (error) {
    // 对响应错误做点什么
    if (error.response) {
      // 请求成功并收到响应，但是状态码超出了 2xx 到范围
      const { status, config } = error.response;
      if (status === 401) {
        // 去除重复调用刷新token接口
        if (!isRefreshToken) {
          const userLoginInfo = store.getters['user/userLoginInfo'];
          // 如果用户登陆信息不存在，直接跳转登陆页
          if (userLoginInfo) {
            // 将状态置为 token 刷新中
            isRefreshToken = true;
            const { refresh_token: refreshtoken } = userLoginInfo;
            // 调用token刷新接口，获取用户登陆信息
            return refreshToken({ refreshtoken })
              .then(({ data }) => {
                // 重置接口调用失败
                if (!data.success) {
                  throw new Error('token 刷新失败');
                }
                // 更新用户登录信息
                store.commit('user/setUserLoginInfo', JSON.parse(data.content));
                // 将requests数组中挂起的请求，重新发送出去
                requests.forEach((cb) => cb());
                // 将数组重置
                requests = [];
                // 将上一次的请求，重新发送
                return request(config);
              })
              .catch(() => {
                redirectToLogin();
              })
              .finally(() => {
                // 将状态重置
                isRefreshToken = false;
              });
          } else {
            redirectToLogin();
          }
        }
        // 在刷新状态下时，将请求的接口挂起到requests数组中
        return new Promise((resolve) =>
          requests.push(() => resolve(request(config))),
        );
      } else if (status === 404) {
        Message.error('请求资源不存在');
      } else {
        Message.error('接口请求失败，请联系管理员');
      }
    } else if (error.request) {
      // 请求成功并未收到响应
      Message.error('请求超时');
      console.log(error.request);
    } else {
      // 设置请求时触发的某些错误
      Message.error('请求错误');
      console.log('Error', error.message);
    }
    return Promise.reject(error);
  },
);

const post = (url: string, data: object) => {
  return request({
    url,
    method: 'POST',
    data: qs.stringify(data),
    headers: { 'content-type': 'application/x-www-form-urlencoded' },
  });
};

const get = (url: string, params: object) => {
  return request({
    url,
    method: 'GET',
    params,
    headers: { 'content-type': 'application/x-www-form-urlencoded' },
  });
};

export { post, get };

export default request;
```
