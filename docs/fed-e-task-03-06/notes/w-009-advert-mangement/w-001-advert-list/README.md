# 广告管理

## 功能

- 添加/编辑广告
- 广告上/下线

## 添加/编辑广告

### 新增接口 API

`src/services/space/index.ts`

```ts
/**
 * 获取所有的广告位
 */
export const getAllSpaces = () => {
  return get(`/front/ad/space/getAllSpaces`);
};

/**
 * 根据Id获取广告信息
 */
export const getAdById = (id: number | string) => {
  return get(`/front/ad/getAdById`, { id });
};

/**
 * 新增或者修改广告信息
 * @param data
 */
export const saveOrUpdate = (data: object) => {
  return request({
    url: '/front/ad/saveOrUpdate',
    method: 'POST',
    data,
  });
};
```

### 组件封装

`src/views/advert/components/CreateOrEdit.vue`

```vue
<template>
  <el-card class="CreateOrEdit">
    <el-form ref="form" :rules="rules" :model="form" label-width="90px">
      <el-form-item label="广告名称" prop="name">
        <el-input v-model="form.name"></el-input>
      </el-form-item>
      <el-form-item label="广告位置" prop="spaceId">
        <el-select v-model="form.spaceId" placeholder="请选择">
          <el-option
            v-for="item in spaces"
            :key="item.id"
            :label="item.name"
            :value="item.id"
          ></el-option>
        </el-select>
      </el-form-item>
      <el-form-item label="开始时间" prop="startTime">
        <el-date-picker
          type="date"
          placeholder="选择日期"
          v-model="form.startTime"
          style="width: 100%;"
        />
      </el-form-item>
      <el-form-item label="到期时间" prop="endTime">
        <el-date-picker
          type="date"
          placeholder="选择日期"
          v-model="form.endTime"
          style="width: 100%;"
        />
      </el-form-item>
      <el-form-item label="上线/下线" prop="categoryId">
        <el-switch
          v-model="form.status"
          :active-value="1"
          :inactive-value="0"
          active-color="#13ce66"
          inactive-color="#ff4949"
        />
      </el-form-item>
      <el-form-item label="广告图片">
        <course-image v-model="form.img" :limit="5" />
      </el-form-item>
      <el-form-item label="广告链接" prop="link">
        <el-input v-model="form.link"></el-input>
      </el-form-item>
      <el-form-item label="广告备注" prop="text">
        <el-input v-model="form.text"></el-input>
      </el-form-item>
      <el-form-item>
        <el-button type="primary" @click="onSubmit">提交</el-button>
        <el-button v-if="!isEdit" @click="resetForm">重置</el-button>
      </el-form-item>
    </el-form>
  </el-card>
</template>

<script lang="ts">
import Vue from 'vue';
import { Form } from 'element-ui';
import CourseImage from '@/components/course-image/index.vue';
import { getAdById, getAllSpaces, saveOrUpdate } from '@/services/space';

export default Vue.extend({
  components: { CourseImage },
  name: 'CreateOrEdit',
  props: {
    isEdit: Boolean,
  },
  data() {
    return {
      spaces: [],
      form: {
        name: '',
        spaceId: '',
        img: '',
        link: '',
        startTime: '',
        endTime: '',
        status: 0,
        text: '',
      },
      rules: {
        name: [
          {
            required: true,
            message: '请输入广告名称',
            trigger: 'blur',
          },
        ],
        startTime: [
          {
            required: true,
            message: '请选择开始时间',
            trigger: 'blur',
          },
        ],
        endTime: [
          {
            required: true,
            message: '请选择到期时间',
            trigger: 'blur',
          },
        ],
        link: [
          {
            required: true,
            message: '请输入广告链接',
            trigger: 'blur',
          },
        ],
      },
    };
  },
  methods: {
    resetForm() {
      console.log(this.$refs.form as Form);
      (this.$refs.form as Form).resetFields();
    },
    onSubmit() {
      (this.$refs.form as Form).validate((valid: boolean) => {
        if (valid) {
          saveOrUpdate(this.form)
            .then(() => {
              this.$message.success(this.isEdit ? '修改成功' : '添加成功');
              this.$router.go(-1);
            })
            .catch((e) => this.$message.error(e));
        } else {
          return false;
        }
      });
    },
    async loadAllSpaces() {
      try {
        this.spaces = await getAllSpaces();
        console.log(this.spaces);
      } catch (e) {
        this.$message.error(e);
      }
    },
    async loadAdInfo() {
      try {
        this.form = await getAdById(this.$route.params.id);
      } catch (e) {
        this.$message.error(e);
      }
    },
  },
  created() {
    this.loadAllSpaces();
    this.isEdit && this.loadAdInfo();
  },
});
</script>
```

### 页面

#### 添加广告

`src/views/advert/add.vue`

```vue
<template>
  <div class="add">
    <create-or-edit />
  </div>
</template>

<script lang="ts">
import Vue from 'vue';
import CreateOrEdit from '@/views/advert/components/CreateOrEdit.vue';

export default Vue.extend({
  components: { CreateOrEdit },
  name: 'advertAdd',
});
</script>
```

#### 编辑广告

`src/views/advert/edit.vue`

```vue
<template>
  <div class="edit">
    <create-or-edit is-edit />
  </div>
</template>

<script lang="ts">
import Vue from 'vue';
import CreateOrEdit from '@/views/advert/components/CreateOrEdit.vue';

export default Vue.extend({
  components: { CreateOrEdit },
  name: 'advertEdit',
});
</script>
```

## 广告列表展示和上/下线广告

### 新增接口 API

`src/services/space/index.ts`

```ts
/**
 * 获取所有的广告列表
 */
export const getAdList = () => {
  return get(`/front/ad/getAdList`);
};

/**
 * 更新广告的状态
 */
export const updateStatus = (params: object) => {
  return get(`/front/ad/updateStatus`, params);
};
```

### 页面

`src/views/advert/index.vue`

```vue
<template>
  <div class="advert">
    <el-button
      style="margin-bottom: 20px"
      type="primary"
      icon="el-icon-plus"
      @click="$router.push('advert/add')"
    >
      添加广告
    </el-button>
    <el-table :data="adList" style="width: 100%">
      <el-table-column align="center" label="ID" prop="id" />
      <el-table-column
        align="center"
        label="广告名称"
        prop="name"
        min-width="180"
      />
      <el-table-column
        align="center"
        label="广告位置"
        prop="spaceId"
        min-width="180"
      >
        <template slot-scope="scope">
          <span>{{ spaces[scope.row.spaceId] }}</span>
        </template>
      </el-table-column>
      <el-table-column align="center" label="头像" prop="img">
        <template slot-scope="scope">
          <img
            v-if="scope.row.img"
            style="width: 30px;height: 30px"
            :src="scope.row.img"
            alt=""
          />
        </template>
      </el-table-column>
      <el-table-column align="center" label="上线/下线" prop="status">
        <template slot-scope="scope">
          <el-switch
            v-model="scope.row.status"
            :active-value="1"
            :inactive-value="0"
            active-color="#13ce66"
            inactive-color="#ff4949"
            @change="onChange(scope.row)"
          />
        </template>
      </el-table-column>
      <el-table-column
        align="center"
        label="时间"
        prop="createdTime"
        min-width="180"
      >
        <template slot-scope="scope">
          {{ scope.row.createdTime | date }}
        </template>
      </el-table-column>
      <el-table-column align="center" label="操作" min-width="180">
        <template slot-scope="scope">
          <el-button
            type="primary"
            size="mini"
            @click="
              $router.push({
                name: 'advertEdit',
                params: {
                  id: scope.row.id,
                },
              })
            "
          >
            编辑
          </el-button>
        </template>
      </el-table-column>
    </el-table>
  </div>
</template>

<script lang="ts">
import Vue from 'vue';
import { getAdList, getAllSpaces, updateStatus } from '@/services/space';

export default Vue.extend({
  name: 'AdvertIndex',
  data() {
    return {
      adList: [],
      spaces: {} as any,
    };
  },
  methods: {
    async loadAdList() {
      try {
        this.adList = await getAdList();
        console.log(this.adList);
      } catch (e) {
        this.$message.error(e);
      }
    },
    async loadAllSpaces() {
      try {
        const data = await getAllSpaces();
        (data as object[]).forEach(
          (value: any) => (this.spaces[value.id] = value.name),
        );
        console.log(this.spaces);
      } catch (e) {
        this.$message.error(e);
      }
    },
    async onChange(ad: any) {
      const { id, status } = ad;
      try {
        await updateStatus({ id, status });
        this.$message.success(status === 0 ? '下线成功' : '上线成功');
      } catch (e) {
        ad.status = status === 0 ? 1 : 0;
        this.$message.error(e);
      }
    },
  },
  created() {
    this.loadAllSpaces();
    this.loadAdList();
  },
});
</script>
```
