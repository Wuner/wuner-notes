# 课程管理

## 功能

- 按条件分页查询课程列表
- 添加/编辑课程
- 添加/编辑/更新阶段
- 添加/编辑/更新课时
- 拖拽排序阶段/课时
- 上传视频

## 新建/编辑课程

### 新增数据类型

`src/types/index.d.ts`

```ts
export interface TeacherDTO {
  id?: number; // 讲师ID
  courseId?: number; // 课程ID
  teacherName?: string; // 讲师名称
  teacherHeadPicUrl?: string; // 讲师头像路径
  position?: string; // 定位
  description?: string; // 描述
}

export interface ActivityCourseDTO {
  id?: number; // 活动课程id
  courseId?: number; // 课程id
  beginTime?: string; // 活动开始时间
  endTime?: string; // 活动结束时间
  amount?: number; // 活动价格
  stock?: number; // 	库存值
}

export interface Course {
  id?: number; // 课程ID
  courseName?: string; // 课程名
  brief?: string; // 课程一句话简介
  teacherDTO?: TeacherDTO; // 讲师信息
  courseDescriptionMarkDown?: string; // 课程描述
  price?: number; // 原价
  discounts?: number; // 优惠价
  priceTag?: string; // 原价标签
  discountsTag?: string; // 优惠标签
  isNew?: true; // 是否新品
  isNewDes?: string; // 广告语
  courseListImg?: string; // 课程列表展示图片
  courseImgUrl?: string; // 解锁封面
  sortNum?: number; // 课程排序，用于后台保存草稿时用到
  previewFirstField?: string; // 课程预览第一个字段
  previewSecondField?: string; // 课程预览第二个字段
  status?: number; // 课程状态，number-草稿，1-上架
  sales?: number; // 显示销量
  activityCourse?: true; // 课程是否做秒杀活动 true是 false不是
  activityCourseDTO?: ActivityCourseDTO; // 活动课程信息
  autoOnlineTime?: string; // 自动上架时间
}
```

### 新增接口 API

`src/services/course/index.ts`

```ts
/**
 * 保存或者更新课程信息
 * @param data
 */
export const saveOrUpdateCourse = (data: Course) => {
  return request({
    url: '/boss/course/saveOrUpdateCourse',
    method: 'POST',
    data,
  });
};

/**
 * 通过课程Id获取课程信息
 * @param courseId 课程ID
 */
export const getCourseById = (courseId: number | string) => {
  return get('/boss/course/getCourseById', { courseId });
};

/**
 * 上传图片
 * @param data
 * @param onUploadProgress
 */
export const uploadCourseImage = (
  data: any,
  onUploadProgress?: (progressEvent: ProgressEvent) => void,
) => {
  // 该接口要求的请求数据类型是：multipart/form-data
  // 所以需要提交 FormData 数据对象
  return request({
    method: 'POST',
    url: '/boss/course/upload',
    data,
    // HTML5 新增的上传响应事件：progress
    onUploadProgress,
  });
};
```

### 组件封装

#### 上传图片组件

`src/components/course-image/index.vue`

```vue
<template>
  <div class="course-image">
    <el-progress
      v-if="isUploading"
      type="circle"
      :percentage="percentage"
      :width="178"
      :status="percentage === 100 ? 'success' : undefined"
    />
    <el-upload
      v-else
      class="avatar-uploader"
      action="https://jsonplaceholder.typicode.com/posts/"
      :show-file-list="false"
      :before-upload="beforeAvatarUpload"
      :http-request="handleUpload"
    >
      <img v-if="value" :src="value" class="avatar" />
      <i v-else class="el-icon-plus avatar-uploader-icon"></i>
    </el-upload>
  </div>
</template>

<script lang="ts">
import Vue from 'vue';
import { uploadCourseImage } from '@/services/course';

export default Vue.extend({
  name: 'CourseImage',
  props: {
    value: {
      type: String,
    },
    limit: {
      type: Number,
      default: 2,
    },
  },
  data() {
    return {
      isUploading: false,
      percentage: 0,
    };
  },
  methods: {
    beforeAvatarUpload(file: any) {
      const isJPG = file.type === 'image/jpeg';
      const isLt2M = file.size / 1024 / 1024 < this.limit;

      if (!isJPG) {
        this.$message.error('上传头像图片只能是 JPG 格式!');
      }
      if (!isLt2M) {
        this.$message.error(`上传头像图片大小不能超过 ${this.limit}MB!`);
      }
      return isJPG && isLt2M;
    },

    async handleUpload(options: any) {
      try {
        this.isUploading = true;
        const fd = new FormData();
        fd.append('file', options.file);
        const data = await uploadCourseImage(fd, (e) => {
          this.percentage = Math.floor((e.loaded / e.total) * 100);
        });
        this.isUploading = false;
        this.percentage = 0;
        this.$emit('input', data.name);
      } catch (e) {
        this.$message.error(e);
      }
      this.isUploading = false;
      this.percentage = 0;
    },
  },
});
</script>

<style lang="scss" scoped>
::v-deep .avatar-uploader .el-upload {
  border: 1px dashed #d9d9d9;
  border-radius: 6px;
  cursor: pointer;
  position: relative;
  overflow: hidden;
}

::v-deep .avatar-uploader .el-upload:hover {
  border-color: #409eff;
}

.avatar-uploader-icon {
  font-size: 28px;
  color: #8c939d;
  width: 178px;
  height: 178px;
  line-height: 178px;
  text-align: center;
}

.avatar {
  width: 178px;
  height: 178px;
  display: block;
}
</style>
```

#### 富文本编辑器组件

`src/components/text-editor/index.vue`

```vue
<template>
  <div ref="editor" class="text-editor"></div>
</template>

<script lang="ts">
import Vue from 'vue';
import E from 'wangeditor';
import { uploadCourseImage } from '@/services/course';

export default Vue.extend({
  name: 'TextEditor',
  props: {
    value: {
      type: String,
      default: '',
    },
  },
  data() {
    return {
      editor: null,
    };
  },
  watch: {
    value() {
      (this.editor as any).txt.html(this.value);
    },
  },
  // 组件已经渲染好，可以初始化操作 DOM 了
  mounted() {
    this.initEditor();
  },
  methods: {
    initEditor() {
      const editor = new E(this.$refs.editor as any);
      this.editor = editor as any;
      // 注意：事件监听必须在 create 之前
      editor.config.onchange = (value: string) => {
        this.$emit('input', value);
      };
      editor.create();

      // 注意：设置初始化必须在 create 之后
      editor.config.customUploadImg = function (
        resultFiles: any,
        insertImgFn: any,
      ) {
        // resultFiles 是 input 中选中的文件列表
        // insertImgFn 是获取图片 url 后，插入到编辑器的方法
        // 上传图片，返回结果，将图片插入到编辑器中
        resultFiles.forEach(async (file: File) => {
          try {
            const fd = new FormData();
            fd.append('file', file);
            const { name } = await uploadCourseImage(fd);
            insertImgFn(name);
          } catch (e) {
            console.log(e);
          }
        });
      };
    },
  },
});
</script>
```

#### 创建/编辑课程组件

`src/views/course/components/CreateOrUpdate.vue`

```vue
<template>
  <el-card>
    <div slot="header">
      <el-steps :active="activeStep" simple>
        <el-step
          :title="item.title"
          :icon="item.icon"
          v-for="(item, index) in steps"
          :key="index"
          @click.native="activeStep = index"
        ></el-step>
      </el-steps>
    </div>
    <el-form label-width="80px">
      <div v-show="activeStep === 0">
        <el-form-item label="课程名称">
          <el-input v-model="course.courseName"></el-input>
        </el-form-item>
        <el-form-item label="课程简介">
          <el-input v-model="course.brief"></el-input>
        </el-form-item>
        <el-form-item label="课程概述">
          <el-input
            style="margin-bottom: 10px"
            v-model="course.previewFirstField"
            type="textarea"
            placeholder="概述1"
          ></el-input>
          <el-input
            v-model="course.previewSecondField"
            type="textarea"
            placeholder="概述2"
          ></el-input>
        </el-form-item>
        <el-form-item label="讲师姓名">
          <el-input v-model="course.teacherDTO.teacherName"></el-input>
        </el-form-item>
        <el-form-item label="讲师简介">
          <el-input v-model="course.teacherDTO.description"></el-input>
        </el-form-item>
        <el-form-item label="课程排序">
          <el-input-number
            v-model="course.sortNum"
            label="描述文字"
          ></el-input-number>
        </el-form-item>
      </div>
      <div v-show="activeStep === 1">
        <el-form-item label="课程封面">
          <!--
            upload 上传文件组件，它支持自动上传，你只需要把上传需要参数配置一下就可以了
            -->
          <!--
            1. 组件需要根据绑定的数据进行图片预览
            2. 组件需要把上传成功的图片地址同步到绑定的数据中
            v-model 的本质还是父子组件通信
              1. 它会给子组件传递一个名字叫 value 的数据（Props）
              2. 默认监听 input 事件，修改绑定的数据（自定义事件）
            -->
          <course-image v-model="course.courseListImg" :limit="5" />
        </el-form-item>
        <el-form-item label="介绍封面">
          <course-image :limit="5" v-model="course.courseImgUrl" />
        </el-form-item>
      </div>
      <div v-show="activeStep === 2">
        <el-form-item label="售卖价格">
          <el-input v-model.number="course.discounts" type="number">
            <template slot="append">元</template>
          </el-input>
        </el-form-item>
        <el-form-item label="商品原价">
          <el-input v-model.number="course.price" type="number">
            <template slot="append">元</template>
          </el-input>
        </el-form-item>
        <el-form-item label="销量">
          <el-input v-model.number="course.sales" type="number">
            <template slot="append">单</template>
          </el-input>
        </el-form-item>
        <el-form-item label="活动标签">
          <el-input v-model="course.discountsTag"></el-input>
        </el-form-item>
      </div>
      <div v-show="activeStep === 3">
        <el-form-item label="限时秒杀开关">
          <el-switch
            v-model="course.activityCourse"
            active-color="#13ce66"
            inactive-color="#ff4949"
          >
          </el-switch>
        </el-form-item>
        <template v-if="course.activityCourse">
          <el-form-item label="开始时间">
            <el-date-picker
              v-model="course.activityCourseDTO.beginTime"
              type="date"
              placeholder="选择日期时间"
              value-format="yyyy-MM-dd"
            />
          </el-form-item>
          <el-form-item label="结束时间">
            <el-date-picker
              v-model="course.activityCourseDTO.endTime"
              type="date"
              placeholder="选择日期时间"
              value-format="yyyy-MM-dd"
            />
          </el-form-item>
          <el-form-item label="秒杀价">
            <el-input
              v-model.number="course.activityCourseDTO.amount"
              type="number"
            >
              <template slot="append">元</template>
            </el-input>
          </el-form-item>
          <el-form-item label="秒杀库存">
            <el-input
              v-model.number="course.activityCourseDTO.stock"
              type="number"
            >
              <template slot="append">个</template>
            </el-input>
          </el-form-item>
        </template>
      </div>
      <div v-show="activeStep === 4">
        <el-form-item label="课程详情">
          <text-editor v-model="course.courseDescriptionMarkDown" />
          <!-- <el-input v-model="course.courseDescriptionMarkDown" type="textarea"></el-input> -->
        </el-form-item>
        <el-form-item label="是否发布">
          <el-switch
            v-model="course.status"
            :active-value="1"
            :inactive-value="0"
            active-color="#13ce66"
            inactive-color="#ff4949"
          />
        </el-form-item>
        <el-form-item>
          <el-button type="primary" @click="handleSave">保存</el-button>
        </el-form-item>
      </div>
      <el-form-item v-if="activeStep >= 0 && activeStep < 4">
        <el-button @click="activeStep++">下一步</el-button>
      </el-form-item>
    </el-form>
  </el-card>
</template>

<script lang="ts">
import Vue from 'vue';
import { saveOrUpdateCourse, getCourseById } from '@/services/course';
import CourseImage from '@/components/course-image/index.vue';
import TextEditor from '@/components/text-editor/index.vue';
import dayjs from 'dayjs';
import { Course } from '@/types';

export default Vue.extend({
  name: 'CreateOrUpdateCourse',
  props: {
    isEdit: {
      type: Boolean,
      default: false,
    },
    courseId: {
      type: [String, Number],
    },
  },
  components: {
    CourseImage,
    TextEditor,
  },
  data() {
    return {
      activeStep: 0,
      steps: [
        { title: '基本信息', icon: 'el-icon-edit' },
        { title: '课程封面', icon: 'el-icon-edit' },
        { title: '销售信息', icon: 'el-icon-edit' },
        { title: '秒杀活动', icon: 'el-icon-edit' },
        { title: '课程详情', icon: 'el-icon-edit' },
      ],
      course: ({
        courseName: '',
        brief: '',
        teacherDTO: {
          teacherName: '',
          teacherHeadPicUrl: '',
          position: '',
          description: '',
        },
        courseDescriptionMarkDown: '',
        price: 0,
        discounts: 0,
        priceTag: '',
        discountsTag: '',
        isNew: true,
        isNewDes: '',
        courseListImg: '',
        courseImgUrl: '',
        sortNum: 0,
        previewFirstField: '',
        previewSecondField: '',
        status: 0,
        sales: 0,
        activityCourse: false,
        activityCourseDTO: {
          beginTime: '',
          endTime: '',
          amount: 0,
          stock: 0,
        },
        autoOnlineTime: '',
      } as unknown) as Course,
    };
  },
  created() {
    if (this.isEdit) {
      this.loadCourse();
    }
  },
  methods: {
    async loadCourse() {
      try {
        const data = await getCourseById(this.courseId);
        const { activityCourseDTO } = data;
        if (activityCourseDTO) {
          activityCourseDTO.beginTime = dayjs(
            activityCourseDTO.beginTime,
          ).format('YYYY-MM-DD');
          activityCourseDTO.endTime = dayjs(activityCourseDTO.endTime).format(
            'YYYY-MM-DD',
          );
        }
        this.course = data;
      } catch (e) {
        this.$message.error(e);
      }
    },

    async handleSave() {
      try {
        await saveOrUpdateCourse(this.course);
        this.$router.go(-1);
        this.$message.success('保存成功');
      } catch (e) {
        this.$message.error(e);
      }
    },
  },
});
</script>

<style lang="scss" scoped>
.el-step {
  cursor: pointer;
}
</style>
```

### 页面

#### 新建课程

`src/views/course/create.vue`

```vue
<template>
  <div class="course-create">
    <create-or-update />
  </div>
</template>

<script lang="ts">
import Vue from 'vue';
import CreateOrUpdate from './components/CreateOrUpdate.vue';

export default Vue.extend({
  name: 'CourseCreate',
  components: {
    CreateOrUpdate,
  },
});
</script>
```

#### 编辑课程

`src/views/course/edit.vue`

```vue
<template>
  <div class="course-create">
    <create-or-update is-edit :course-id="courseId" />
  </div>
</template>

<script lang="ts">
import Vue from 'vue';
import CreateOrUpdate from './components/CreateOrUpdate.vue';

export default Vue.extend({
  name: 'CourseEdit',
  props: {
    courseId: {
      type: [String, Number],
      required: true,
    },
  },
  components: {
    CreateOrUpdate,
  },
});
</script>
```

## 课程列表展示和上下架课程

### 新增数据类型

`src/types/index.d.ts`

```ts
export interface CourseFilter {
  currentPage: number; // 当前页码
  pageSize: number; // 一页数量
  courseName: string; // 课程名称
  status: number | string; // 课程上下架状态
}
```

### 新增接口 API

```ts
/**
 * 分页查询课程信息
 * @param data
 */
export const getQueryCourses = (data: CourseFilter) => {
  return request({
    url: '/boss/course/getQueryCourses',
    method: 'POST',
    data,
  });
};

/**
 * 课程上下架
 * @param params
 */
export const changeState = (params: object) => {
  return get('/boss/course/changeState', params);
};
```

### 组件封装

`src/views/course/components/CourseList.vue`

```vue
<template>
  <div class="course-list">
    <div class="course-list-header">
      <el-form
        ref="form"
        :inline="true"
        :model="courseFilter"
        class="demo-form-inline"
      >
        <el-form-item prop="courseName" label="课程名称">
          <el-input
            v-model="courseFilter.courseName"
            clearable
            placeholder="请输入课程名称"
          ></el-input>
        </el-form-item>
        <el-form-item prop="status" label="状态">
          <el-select v-model="courseFilter.status" placeholder="请选则状态">
            <el-option label="全部" value=""></el-option>
            <el-option label="上架" :value="1"></el-option>
            <el-option label="下架" :value="0"></el-option>
          </el-select>
        </el-form-item>
        <el-form-item>
          <el-button type="primary" @click="onReset">重置</el-button>
          <el-button type="primary" @click="onSelect">查询</el-button>
        </el-form-item>
      </el-form>
      <el-button type="primary" icon="el-icon-plus" @click="toCreate">
        新建课程
      </el-button>
    </div>
    <el-table
      v-loading="loading"
      :data="courses"
      style="width: 100%;margin-top: 20px;"
    >
      <el-table-column align="center" label="ID" prop="id" />
      <el-table-column
        align="center"
        label="课程名称"
        prop="courseName"
        min-width="180"
      />
      <el-table-column align="center" label="价格" prop="price" min-width="180">
        <template slot-scope="scope"> ¥{{ scope.row.price }} </template>
      </el-table-column>
      <el-table-column
        align="center"
        label="排序"
        prop="sortNum"
        min-width="180"
      />
      <el-table-column align="center" label="状态" prop="status">
        <template slot-scope="scope">
          <el-switch
            v-model="scope.row.status"
            :active-value="1"
            :inactive-value="0"
            active-color="#13ce66"
            inactive-color="#ff4949"
            @change="onChange(scope.row)"
          />
        </template>
      </el-table-column>
      <el-table-column align="center" label="操作" min-width="180">
        <template slot-scope="scope">
          <el-button size="mini" @click="handleEdit(scope.row)">编辑</el-button>
          <el-button size="mini" @click="handleSection(scope.row)">
            内容管理
          </el-button>
        </template>
      </el-table-column>
    </el-table>
    <el-pagination
      v-loading="loading"
      style="margin-top: 20px"
      @size-change="handleSizeChange"
      @current-change="handleCurrentChange"
      :current-page="courseFilter.currentPage"
      :page-sizes="[10, 20, 30, 40]"
      :page-size="courseFilter.pageSize"
      layout="total, sizes, prev, pager, next, jumper"
      :total="total"
    >
    </el-pagination>
  </div>
</template>

<script lang="ts">
import Vue from 'vue';
import { changeState, getQueryCourses } from '@/services/course';
import { Course } from '@/types';
import { Form } from 'element-ui';

export default Vue.extend({
  name: 'CourseIndex',
  data() {
    return {
      loading: false,
      courses: [],
      total: 0,
      courseFilter: {
        courseName: '',
        status: '',
        currentPage: 1,
        pageSize: 10,
      },
    };
  },
  methods: {
    onReset() {
      (this.$refs.form as Form).resetFields();
    },
    async onChange(course: Course) {
      try {
        const { id: courseId, status } = course;
        await changeState({ courseId, status });
        this.$message.success(status === 0 ? '下架成功' : '上架成功');
      } catch (e) {
        course.status = course.status === 0 ? 1 : 0;
        this.$message.error(e);
      }
    },
    onSelect() {
      this.courseFilter.currentPage = 1;
      this.loadAllCourses();
    },
    toCreate() {
      this.$router.push('course/create');
    },
    handleEdit(course: Course) {
      this.$router.push({
        name: 'courseEdit',
        params: {
          courseId: course.id + '',
        },
      });
    },
    handleSection(course: Course) {
      this.$router.push({
        name: 'courseSection',
        params: {
          courseId: course.id + '',
        },
      });
    },
    async loadAllCourses() {
      this.loading = true;
      try {
        const { records, total } = await getQueryCourses(this.courseFilter);
        this.courses = records;
        this.total = total;
        console.log(records);
      } catch (e) {
        this.$message.error(e);
      }
      this.loading = false;
    },
    handleSizeChange(size: number) {
      this.courseFilter.currentPage = 1;
      this.courseFilter.pageSize = size;
      this.loadAllCourses();
    },
    handleCurrentChange(current: number) {
      this.courseFilter.currentPage = current;
      this.loadAllCourses();
    },
  },
  created() {
    this.loadAllCourses();
  },
});
</script>

<style lang="scss" scoped>
.course-list {
  &-header {
    display: flex;
    align-items: center;
    justify-content: space-between;
    .el-form-item {
      margin-bottom: 0;
    }
  }
}
</style>
```

### 页面

```vue
<template>
  <div class="course">
    <course-list />
  </div>
</template>

<script lang="ts">
import Vue from 'vue';
import CourseList from './components/CourseList.vue';

export default Vue.extend({
  components: { CourseList },
  name: 'CourseIndex',
});
</script>
```

## 内容管理

### 新增接口 API

`src/services/course/section.ts`

```ts
import request, { get } from '@/utils/request';
import { Section } from '@/types';

/**
 * 保存或者更新章节内容
 * @param data
 */
export const saveOrUpdateSection = (data: Section) => {
  return request({
    method: 'POST',
    url: '/boss/course/section/saveOrUpdateSection',
    data,
  });
};

/**
 * 获取章节内容
 * @param sectionId
 */
export const getSectionById = (sectionId: number | string) => {
  return get('/boss/course/section/getBySectionId', { sectionId });
};

export const getSectionAndLesson = (courseId: number | string) => {
  return get('/boss/course/section/getSectionAndLesson', { courseId });
};
```

`src/services/course/lesson.ts`

```ts
import { Lesson } from '@/types';
import request from '@/utils/request';

/**
 * 保存或者更新课时
 * @param data
 */
export const saveOrUpdateLesson = (data: Lesson) => {
  return request({
    method: 'POST',
    url: '/boss/course/lesson/saveOrUpdate',
    data,
  });
};
```

`src/services/aliyun-oss/index.ts`

```ts
/**
 * 阿里云上传
 */

import request, { get } from '@/utils/request';

/**
 * 获取媒体信息
 */
export const getMediaByLessonId = (lessonId: string | number) => {
  return get('/boss/course/upload/getMediaByLessonId.json', { lessonId });
};

/**
 * 获取阿里云图片上传凭证
 */
export const aliyunImagUploadAddressAdnAuth = () => {
  return get('/boss/course/upload/aliyunImagUploadAddressAdnAuth.json');
};

/**
 * 获取阿里云视频上传凭证
 * @param params
 */
export const aliyunVideoUploadAddressAdnAuth = (params: object) => {
  return get(
    '/boss/course/upload/aliyunVideoUploadAddressAdnAuth.json',
    params,
  );
};

/**
 * 阿里云转码请求
 * @param data
 */
export const transCodeVideo = (data: object) => {
  return request({
    method: 'POST',
    url: '/boss/course/upload/aliyunTransCode.json',
    data,
  });
};

/**
 * 阿里云转码进度
 * @param lessonId
 */
export const getAliyunTransCodePercent = (lessonId: string | number) => {
  return get('/boss/course/upload/aliyunTransCodePercent.json', { lessonId });
};
```

### 页面

#### 数据展示和操作

```vue
<template>
  <div class="course-section">
    <!-- 阶段列表 -->
    <el-card>
      <div class="card-header" slot="header">
        {{ course.courseName }}
        <el-button type="primary" @click="handleShowAddSection"
          >添加阶段</el-button
        >
      </div>
      <el-tree
        :data="sections"
        :props="defaultProps"
        draggable
        :allow-drop="handleAllowDrop"
        v-loading="isLoading"
        @node-drop="handleSort"
      >
        <div class="inner" slot-scope="{ node, data }">
          <span>{{ node.label }}</span>
          <!-- section -->
          <span v-if="data.sectionName" class="actions">
            <el-button @click.stop="handleEditSectionShow(data)"
              >编辑</el-button
            >
            <el-button type="primary" @click.stop="handleShowAddLesson(data)"
              >添加课时</el-button
            >
            <el-select
              class="select-status"
              v-model="data.status"
              placeholder="请选择"
              @change="handleSectionStatusChange(data)"
            >
              <el-option label="已隐藏" :value="0" />
              <el-option label="待更新" :value="1" />
              <el-option label="已更新" :value="2" />
            </el-select>
          </span>
          <!-- lession -->
          <span v-else class="actions">
            <el-button @click="handleShowEditLesson(data, node.parent.data)"
              >编辑</el-button
            >
            <el-button
              type="success"
              @click="
                $router.push({
                  name: 'courseVideo',
                  params: {
                    courseId,
                  },
                  query: {
                    sectionId: node.parent.id,
                    lessonId: data.id,
                    courseName: course.courseName,
                    sectionName: node.parent.data.sectionName,
                    lessonName: data.theme,
                  },
                })
              "
            >
              上传视频
              {{ data.sectionName }}
            </el-button>
            <el-select
              class="select-status"
              v-model="data.status"
              placeholder="请选择"
              @change="handleLessonStatusChange(data)"
            >
              <el-option label="已隐藏" :value="0" />
              <el-option label="待更新" :value="1" />
              <el-option label="已更新" :value="2" />
            </el-select>
          </span>
        </div>
      </el-tree>
    </el-card>
    <!-- /阶段列表 -->

    <!-- 添加阶段 -->
    <el-dialog title="添加课程阶段" :visible.sync="isAddSectionShow">
      <el-form ref="section-form" :model="section" label-width="70px">
        <el-form-item label="课程名称">
          <el-input
            :value="course.courseName"
            autocomplete="off"
            disabled
          ></el-input>
        </el-form-item>
        <el-form-item label="章节名称" prop="sectionName">
          <el-input v-model="section.sectionName" autocomplete="off"></el-input>
        </el-form-item>
        <el-form-item label="章节描述" prop="description">
          <el-input
            v-model="section.description"
            type="textarea"
            autocomplete="off"
          ></el-input>
        </el-form-item>
        <el-form-item label="章节排序" prop="orderNum">
          <el-input-number v-model="section.orderNum"></el-input-number>
        </el-form-item>
      </el-form>
      <div slot="footer" class="dialog-footer">
        <el-button @click="isAddSectionShow = false">取 消</el-button>
        <el-button type="primary" @click="handleAddSection">确 定</el-button>
      </div>
    </el-dialog>
    <!-- /添加阶段 -->

    <!-- 添加课时 -->
    <el-dialog title="添加课时" :visible.sync="isAddLessonShow">
      <el-form ref="lesson-form" :model="lesson" label-width="100px">
        <el-form-item label="课程名称">
          <el-input
            :value="course.courseName"
            autocomplete="off"
            disabled
          ></el-input>
        </el-form-item>
        <el-form-item label="章节名称" prop="sectionName">
          <el-input
            :value="lesson.sectionName"
            disabled
            autocomplete="off"
          ></el-input>
        </el-form-item>
        <el-form-item label="课时名称" prop="sectionName">
          <el-input v-model="lesson.theme" autocomplete="off"></el-input>
        </el-form-item>
        <el-form-item label="时长" prop="description">
          <el-input
            v-model.number="lesson.duration"
            type="number"
            autocomplete="off"
          >
            <template slot="append">分钟</template>
          </el-input>
        </el-form-item>
        <el-form-item label="是否开放试听" prop="description">
          <el-switch v-model="lesson.isFree"></el-switch>
        </el-form-item>
        <el-form-item label="课时排序" prop="description">
          <el-input-number v-model="lesson.orderNum"></el-input-number>
        </el-form-item>
      </el-form>
      <div slot="footer" class="dialog-footer">
        <el-button @click="isAddLessonShow = false">取 消</el-button>
        <el-button type="primary" @click="handleAddLesson">确 定</el-button>
      </div>
    </el-dialog>
    <!-- /添加课时 -->
  </div>
</template>

<script lang="ts">
import Vue from 'vue';
import {
  getSectionAndLesson,
  saveOrUpdateSection,
  getSectionById,
} from '@/services/course/section';
import { getCourseById } from '@/services/course';
import { Form } from 'element-ui';
import { saveOrUpdateLesson } from '@/services/course/lesson';
import { Lesson } from '@/types';

export default Vue.extend({
  name: 'CourseSection',
  props: {
    courseId: {
      type: [String, Number],
      required: true,
    },
  },
  data() {
    const defaultProps = {
      children: 'lessonDTOS',
      label(data: any) {
        return data.sectionName || data.theme;
      },
    };

    const section = {
      courseId: this.courseId || '',
      sectionName: '',
      description: '',
      orderNum: 0,
      status: 0,
    };

    const lesson = {
      courseId: this.courseId || '',
      sectionId: '',
      sectionName: '',
      theme: '',
      duration: 0,
      isFree: false,
      orderNum: 0,
      status: 0,
    };

    return {
      course: {},
      sections: [],
      defaultProps,
      isAddSectionShow: false,
      section,
      isAddLessonShow: false,
      lesson,
      isLoading: false,
    };
  },

  created() {
    this.loadSections();
    this.loadCourse();
  },

  methods: {
    async loadCourse() {
      try {
        this.course = await getCourseById(this.courseId);
      } catch (e) {
        this.$message.error(e);
      }
    },

    async loadSections() {
      try {
        this.sections = await getSectionAndLesson(this.courseId);
      } catch (e) {
        this.$message.error(e);
      }
    },

    handleShowAddSection() {
      this.section = {
        // 防止数据还是编辑时获取的数据
        courseId: this.courseId,
        sectionName: '',
        description: '',
        orderNum: 0,
        status: 0,
      };
      this.isAddSectionShow = true;
    },

    async handleAddSection() {
      await saveOrUpdateSection(this.section);
      this.loadSections();
      this.isAddSectionShow = false;
      (this.$refs['section-form'] as Form).resetFields();
      this.$message.success('操作成功');
    },

    async handleEditSectionShow(section: any) {
      try {
        this.section = await getSectionById(section.id);
      } catch (e) {
        this.$message.error(e);
      }
      this.isAddSectionShow = true;
    },

    async handleSectionStatusChange(section: any) {
      await saveOrUpdateSection(section);
      this.$message.success('操作成功');
    },

    async handleLessonStatusChange(lesson: any) {
      await saveOrUpdateLesson(lesson);
      this.$message.success('操作成功');
    },

    handleShowAddLesson(data: any) {
      console.log(data);
      this.lesson = {
        sectionName: data.sectionName,
        sectionId: data.id,
        courseId: this.courseId,
        theme: '',
        duration: 0,
        isFree: false,
        orderNum: 0,
        status: 0,
      };
      this.isAddLessonShow = true;
    },

    async handleAddLesson() {
      await saveOrUpdateLesson(this.lesson as Lesson);
      this.$message.success('操作成功');
      this.loadSections();
      this.isAddLessonShow = false;
    },

    handleShowEditLesson(lesson: any, section: any) {
      console.log(lesson, section);
      this.lesson = lesson;
      this.lesson.sectionName = section.sectionName;
      this.isAddLessonShow = true;
    },

    handleAllowDrop(draggingNode: any, dropNode: any, type: any) {
      // draggingNode 拖动的节点
      // dropNode 放置的目标节点
      // type：'prev'、'inner' 和 'next'，分别表示放置在目标节点前、插入至目标节点和放置在目标节点后
      return (
        draggingNode.data.sectionId === dropNode.data.sectionId &&
        type !== 'inner'
      );
    },

    async handleSort(dragNode: any, dropNode: any) {
      this.isLoading = true;
      try {
        await Promise.all(
          dropNode.parent.childNodes.map((item: any, index: number) => {
            if (dragNode.data.lessonDTOS) {
              // 阶段
              return saveOrUpdateSection({
                id: item.data.id,
                orderNum: index + 1,
              });
            } else {
              // 课时
              return saveOrUpdateLesson({
                id: item.data.id,
                orderNum: index + 1,
              } as Lesson);
            }
          }),
        );
        this.$message.success('排序成功');
      } catch (err) {
        console.log(err);
        this.$message.error('排序失败');
      }
      this.isLoading = false;
    },
  },
});
</script>

<style lang="scss" scoped>
.card-header {
  display: flex;
  justify-content: space-between;
  align-items: center;
}

.inner {
  flex: 1;
  padding: 10px;
  display: flex;
  justify-content: space-between;
  align-items: center;
  border-bottom: 1px solid #ebeef5;
}

::v-deep .el-tree-node__content {
  height: auto;
}

.select-status {
  max-width: 100px;
  margin-left: 8px;
}
</style>
```

#### 上传视频

```vue
<template>
  <div class="container">
    <el-card>
      <div slot="header">
        <div>课程：{{ info.courseName }}</div>
        <div>阶段：{{ info.sectionName }}</div>
        <div>课时：{{ info.lessonName }}</div>
      </div>
      <el-form label-width="40px">
        <el-form-item label="视频">
          <el-upload
            v-loading="isUploading"
            :on-change="handleOnChange"
            :auto-upload="false"
            class="upload-demo"
            action="https://jsonplaceholder.typicode.com/posts/"
            :file-list="videoFile"
          >
            <el-button size="small" type="primary">选择视频</el-button>
          </el-upload>
          <span v-if="videoFile.length === 0 && mediaInfo.fileName">
            {{ mediaInfo.fileName }}
          </span>
        </el-form-item>
        <el-form-item label="封面">
          <el-upload
            v-loading="isUploading"
            :on-change="handleOnChangeImg"
            :auto-upload="false"
            class="upload-demo"
            action="https://jsonplaceholder.typicode.com/posts/"
            :file-list="imageFile"
          >
            <el-button size="small" type="primary">选择封面</el-button>
          </el-upload>
          <img
            style="width: 200px"
            v-if="imageFile.length === 0 && mediaInfo.coverImageUrl"
            :src="mediaInfo.coverImageUrl"
            class="avatar"
          />
        </el-form-item>
        <el-form-item v-if="isUploading">
          <el-progress
            type="circle"
            :percentage="percentage"
            :width="178"
            :status="percentage === 100 ? 'success' : undefined"
          />
        </el-form-item>
        <el-form-item v-loading="isUploading">
          <el-button type="primary" @click="authUpload">开始上传</el-button>
          <el-button>返回</el-button>
        </el-form-item>
      </el-form>
    </el-card>
  </div>
</template>
<script>
/* eslint-disable */
import {
  getMediaByLessonId,
  aliyunImagUploadAddressAdnAuth,
  aliyunVideoUploadAddressAdnAuth,
  transCodeVideo,
  getAliyunTransCodePercent,
} from '@/services/aliyun-oss';

export default {
  props: {
    courseId: String,
  },
  data() {
    return {
      videoFile: [],
      imageFile: [],
      isUploading: false,
      percentage: 0,
      uploader: null,
      videoId: null,
      imageUrl: '',
      fileName: '',
      mediaInfo: {},
      info: {},
    };
  },
  created() {
    this.loadInfo();
    this.initUploader();
  },
  methods: {
    handleOnChange(file) {
      this.videoFile = [file];
    },
    handleOnChangeImg(file) {
      this.imageFile = [file];
    },
    authUpload() {
      const videoFile = this.videoFile[0];
      const imageFile = this.imageFile[0];
      if (imageFile && videoFile) {
        this.uploader.addFile(videoFile.raw, null, null, null, '{"Vod":{}}');
        this.uploader.addFile(imageFile.raw, null, null, null, '{"Vod":{}}');
        this.fileName = videoFile.raw.name;
        this.uploader.startUpload();
      } else {
        this.$message.error('请选择视频和封面');
      }
    },
    initUploader() {
      this.uploader = new window.AliyunUpload.Vod({
        // 阿里账号ID，必须有值 ，值的来源https://help.aliyun.com/knowledge_detail/37196.html
        userId: 1618139964448548,
        // 上传到点播的地域， 默认值为'cn-shanghai',//eu-central-1,ap-southeast-1
        region: 'cn-shanghai',
        // 分片大小默认1M，不能小于100K
        partSize: 1048576,
        // 并行上传分片个数，默认5
        parallel: 5,
        // 网络原因失败时，重新上传次数，默认为3
        retryCount: 3,
        // 网络原因失败时，重新上传间隔时间，默认为2秒
        retryDuration: 2,
        // 开始上传
        onUploadstarted: async (uploadInfo) => {
          console.log('onUploadstarted', uploadInfo);
          let uploadAuthInfo = null;
          if (uploadInfo.isImage) {
            const data = await aliyunImagUploadAddressAdnAuth();
            console.log(data);
            this.imageUrl = data.imageURL;
            uploadAuthInfo = data;
          } else {
            const data = await aliyunVideoUploadAddressAdnAuth({
              fileName: uploadInfo.file.name,
            });
            this.videoId = data.videoId;
            uploadAuthInfo = data;
          }

          // console.log('uploadAuthInfo', uploadAuthInfo)

          this.uploader.setUploadAuthAndAddress(
            uploadInfo,
            uploadAuthInfo.uploadAuth,
            uploadAuthInfo.uploadAddress,
            uploadAuthInfo.videoId || uploadAuthInfo.imageId,
          );
        },
        // 文件上传成功
        onUploadSucceed: function (uploadInfo) {
          console.log('onUploadSucceed', uploadInfo);
        },
        // 文件上传失败
        onUploadFailed: function (uploadInfo, code, message) {
          console.log('onUploadFailed');
        },
        // 文件上传进度，单位：字节
        onUploadProgress: function (uploadInfo, totalSize, loadedPercent) {},
        // 上传凭证超时
        onUploadTokenExpired: function (uploadInfo) {
          console.log('onUploadTokenExpired');
        },
        // 全部文件上传结束
        onUploadEnd: async (uploadInfo) => {
          console.log(uploadInfo);
          console.log({
            lessonId: this.$route.query.lessonId,
            fileId: this.videoId,
            coverImageUrl: this.imageUrl,
            fileName: this.fileName,
          });
          const data = await transCodeVideo({
            lessonId: this.$route.query.lessonId,
            fileId: this.videoId,
            coverImageUrl: this.imageUrl,
            fileName: this.fileName,
          });
          console.log(data);
          this.isUploading = true;

          const timer = setInterval(async () => {
            const data = await getAliyunTransCodePercent(
              this.$route.query.lessonId,
            );
            this.percentage = data;
            if (data === 100) {
              clearInterval(timer);
              this.isUploading = false;
              this.$router.go(-1);
            }
            console.log('转码进度', data);
          }, 3000);
        },
      });
    },
    async loadInfo() {
      this.mediaInfo = await getMediaByLessonId(this.$route.query.lessonId);
      const { courseName, sectionName, lessonName } = this.$route.query;
      this.info = { courseName, sectionName, lessonName };
      console.log(this.info);
    },
  },
};
</script>
```
