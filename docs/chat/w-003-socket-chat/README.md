# socket 服务端聊天开发

- index.js

```js
const express = require('express');
const app = express();
const http = require('http');
const server = http.createServer(app);
const { Server } = require('socket.io');
const io = new Server(server);
let rooms = {};

// 监听连接
io.on('connection', (socket) => {
  console.log('a user connected');

  // 监听创建聊天室
  socket.on('createRoom', (data) => {
    // 将聊天室id存放到rooms对象中
    Object.assign(rooms, data);
    console.log(data);
  });

  // 监听发送
  socket.on('send', (data) => {
    const { to, from, message } = data;
    // 将取到的数据，通过receive发送到对应的聊天室
    socket.to(rooms[to]).emit('receive', { from, message });
  });
});

server.listen(3000, () => {
  console.log('listening on *:3000');
});
```

## [项目地址](https://gitee.com/Wuner/socket-chat-back/tree/init/)
