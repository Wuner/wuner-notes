# 搭建 socket 客户端环境

## 初始⽬录结构说明

```text
├── build 项目构建配置
│   ├── updateAdapter.js 更改adapter的配置
│   ├── webpack.base.conf.js webpack公共配置
│   ├── webpack.dev.conf.js webpack测试打包配置
│   ├── webpack.local.conf.js webpack本地打包配置
│   └── webpack.prod.conf.js webpack生产打包配置
├── cache-loader 项目构建缓存
├── config 项目构建配置内容
├── dist 项目构建生成的包
├── node_modules 安装依赖包存储的地方
├── src
│   ├── api 接口定义的地方
│   ├── assets 静态资源以及样式文件存放点
│   ├── components 公共组件
│   ├── router 路由配置
│   ├── store vuex（状态管理）
│   ├── types ts类型定义存放点
│   ├── utils 公共能力
│   ├── view 视图
│   ├── App.vue 根组件
│   └── main.ts 应用配置
├── static 静态资源存储目录，该目录中的资源不做构建处理
├── .babelrc babel配置文件
├── .editorconfig 编码风格配置文件
├── .eslintignore eslint忽略文件
├── .eslintrc.js eslint配置文件
├── .gitignore git忽略文件
├── .postcssrc.js Vue移动端rem适配 —— Postcss的配置文件
├── .prettierrc.js 代码风格格式化配置文件
├── index.html html模板文件
├── package.json
├── README.md
└── yarn.lock
```

## 初始化项目

### 安装脚手架

这里小编使用自己开发的一款脚手架[@varied/cli](https://www.npmjs.com/package/@varied/cli)，不硬性要求，可使用其他脚手架

```cmd
npm install @varied/cli -g
```

### 创建项目

```cmd
varied init webpack socket-chat
```

![notes](./imgs/1.png)

### 安装依赖

这里我们安装 vant 组件库和 socket 客户端

```cmd
yarn add vant@2.12.50 socket.io-client@4.5.2
```

### vant 配置

这里为了图省事，小编使用全局引入，可自选选择引入方案

- main.js

```js
import Vant from 'vant';
import 'vant/lib/index.css';
Vue.use(Vant);
```

### js 配置

- 在项目根目录下新建`jsconfig.json`
- 使 webstorm 编译器识别`@`

```json
{
  "compilerOptions": {
    "baseUrl": ".",
    "paths": {
      "@/*": ["src/*"],
      "components/*": ["src/components/*"]
    }
  }
}
```

## 运行测试

```cmd
yarn start
# 或者
npm run start
```

![notes](./imgs/2.png)

如图所示，即运行成功
