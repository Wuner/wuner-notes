# 20 个 JS 小技巧

## 1. 声明和初始化数组

我们可以使用默认值（如""、null 或 ）初始化特定大小的数组 0。您可能已经将这些用于一维数组，但如何初始化二维数组/矩阵呢？

```javascript
const array = Array(5).fill('');
// 输出
(5)[('', '', '', '', '')];

const matrix = Array(5)
  .fill(0)
  .map(() => Array(5).fill(0));
// 输出
// (5) [Array(5), Array(5), Array(5), Array(5), Array(5)]
// 0: (5) [0, 0, 0, 0, 0]
// 1: (5) [0, 0, 0, 0, 0]
// 2: (5) [0, 0, 0, 0, 0]
// 3: (5) [0, 0, 0, 0, 0]
// 4: (5) [0, 0, 0, 0, 0]
// length: 5
```

## 2. 找出总和、最小值和最大值

我们应该利用 reduce 方法来快速找到基本的数学运算。

```javascript
const array = [5, 4, 7, 8, 9, 2];
```

- 和

```javascript
array.reduce((a, b) => a + b);
// 输出: 35
```

- 最大限度

```javascript
array.reduce((a, b) => (a > b ? a : b));
// 输出: 9
```

- 最小

```javascript
array.reduce((a, b) => (a < b ? a : b));
// 输出: 2
```

## 3. 对字符串、数字或对象数组进行排序

我们有内置的方法 sort()和 reverse()用于对字符串进行排序，但是数字或对象数组呢？

让我们看看数字和对象的升序和降序排序技巧。

- 排序字符串数组

```javascript
const stringArr = ['Joe', 'Kapil', 'Steve', 'Musk'];
stringArr.sort();
// 输出
(4)[('Joe', 'Kapil', 'Musk', 'Steve')];

stringArr.reverse();
// 输出
(4)[('Steve', 'Musk', 'Kapil', 'Joe')];
```

- 排序数字数组

```javascript
const array = [40, 100, 1, 5, 25, 10];
array.sort((a, b) => a - b);
// 输出
(6)[(1, 5, 10, 25, 40, 100)];

array.sort((a, b) => b - a);
// 输出
(6)[(100, 40, 25, 10, 5, 1)];
```

- 对象数组排序

```javascript
const objectArr = [
  { first_name: 'Lazslo', last_name: 'Jamf' },
  { first_name: 'Pig', last_name: 'Bodine' },
  { first_name: 'Pirate', last_name: 'Prentice' },
];
objectArr.sort((a, b) => a.last_name.localeCompare(b.last_name));
// 输出
// (3) [{…}, {…}, {…}]
// 0: {first_name: "Pig", last_name: "Bodine"}
// 1: {first_name: "Lazslo", last_name: "Jamf"}
// 2: {first_name: "Pirate", last_name: "Prentice"}
// length: 3
```

## 4. 从数组中过滤出虚假值

Falsy 值喜欢 0，undefined，null，false，""，''可以很容易地通过以下方法省略

```javascript
const array = [3, 0, 6, 7, '', false];
array.filter(Boolean);
// 输出
(3)[(3, 6, 7)];
```

## 5. 对各种条件使用逻辑运算符

如果你想减少嵌套 if…else 或 switch case，你可以简单地使用基本的逻辑运算符 AND/OR。

```javascript
function doSomething(arg1) {
  arg1 = arg1 || 10;
  // 如果尚未设置，则将 arg1 设置为 10 作为默认值
  return arg1;
}

let foo = 10;
foo === 10 && doSomething();
// is the same thing as if (foo == 10) then doSomething();
// 输出: 10

foo === 5 || doSomething();
// is the same thing as if (foo != 5) then doSomething();
// 输出: 10
```

## 6. 删除重复值

您可能已经将 indexOf() 与 for 循环一起使用，该循环返回第一个找到的索引或较新的 includes() 从数组中返回布尔值 true/false 以找出/删除重复项。 这是我们有两种更快的方法。

```javascript
const array = [5, 4, 7, 8, 9, 2, 7, 5];
array.filter((item, idx, arr) => arr.indexOf(item) === idx);
// or
const nonUnique = [...new Set(array)];
// 输出: [5, 4, 7, 8, 9, 2]
```

## 7. 创建计数器对象或映射

大多数情况下，需要通过创建计数器对象或映射来解决问题，该对象或映射将变量作为键进行跟踪，并将其频率/出现次数作为值进行跟踪。

```javascript
let string = 'kapilalipak';

const table = {};
for (let char of string) {
  table[char] = table[char] + 1 || 1;
}
// 输出
// {k: 2, a: 3, p: 2, i: 2, l: 2}
```

和

```javascript
const countMap = new Map();
for (let i = 0; i < string.length; i++) {
  if (countMap.has(string[i])) {
    countMap.set(string[i], countMap.get(string[i]) + 1);
  } else {
    countMap.set(string[i], 1);
  }
}
// 输出
// Map(5) {"k" => 2, "a" => 3, "p" => 2, "i" => 2, "l" => 2}
```

## 8. 三元运算符很酷

您可以避免使用三元运算符嵌套条件 if…elseif…elseif。

```javascript
function Fever(temp) {
  return temp > 97
    ? 'Visit Doctor!'
    : temp < 97
    ? 'Go Out and Play!!'
    : 'Take Some Rest!';
}

// 输出
// Fever(97): "Take Some Rest!"
// Fever(100): "Visit Doctor!"
```

## 9. 与旧版相比，for 循环更快

- for 并 for..in 默认为您提供索引，但您可以使用 arr[index]。
- for..in 也接受非数字，所以避免它。
- forEach,for...of 直接获取元素。
- forEach 也可以为您提供索引，但 for...of 不能。
- for 并 for...of 考虑阵列中的孔，但其他 2 个不考虑。

## 10. 合并 2 个对象

通常我们需要在日常任务中合并多个对象。

```javascript
const user = {
  name: 'Kapil Raghuwanshi',
  gender: 'Male',
};
const college = {
  primary: 'Mani Primary School',
  secondary: 'Lass Secondary School',
};
const skills = {
  programming: 'Extreme',
  swimming: 'Average',
  sleeping: 'Pro',
};

const summary = { ...user, ...college, ...skills };

// 输出
// gender: 'Male';
// name: 'Kapil Raghuwanshi';
// primary: 'Mani Primary School';
// programming: 'Extreme';
// secondary: 'Lass Secondary School';
// sleeping: 'Pro';
// swimming: 'Average';
```

## 11. 箭头函数

箭头函数表达式是传统函数表达式的紧凑替代品，但有局限性，不能在所有情况下使用。由于它们具有词法范围（父范围）并且没有自己的范围 this，arguments 因此它们指的是定义它们的环境。

```javascript
const person = {
  name: 'Kapil',
  sayName() {
    return this.name;
  },
};
person.sayName();
// 输出
('Kapil');
```

但

```javascript
const person = {
  name: 'Kapil',
  sayName: () => {
    return this.name;
  },
};
person.sayName();
// 输出
('');
```

## 12. 可选链

可选的链接 ?.如果值在 ? 之前，则停止评估。为 undefined 或 null 并返回

```javascript
undefined;
const user = {
  employee: {
    name: 'Kapil',
  },
};
user.employee?.name;
// 输出: "Kapil"
user.employ?.name;
// 输出: undefined
user.employ.name;
// 输出: VM21616:1 Uncaught TypeError: Cannot read property 'name' of undefined
```

## 13. 打乱数组

利用内置 Math.random()方法。

```javascript
const list = [1, 2, 3, 4, 5, 6, 7, 8, 9];
list.sort(() => {
  return Math.random() - 0.5;
});
// 输出
// (9) [2, 5, 1, 6, 9, 8, 4, 3, 7]
// Call it again
// (9) [4, 1, 7, 5, 3, 8, 2, 9, 6]
```

## 14. 空合并算子

空合并运算符 (??) 是一个逻辑运算符，当其左侧操作数为空或未定义时返回其右侧操作数，否则返回其左侧操作数。

```javascript
const foo = null ?? 'my school';
// 输出: "my school"

const baz = 0 ?? 42;
// 输出: 0
```

## 15. Rest & Spread 运算符

那些神秘的 3 点...可以休息或传播！

```javascript
function myFun(a, b, ...manyMoreArgs) {
  return arguments.length;
}
myFun('one', 'two', 'three', 'four', 'five', 'six');

// 输出: 6
```

和

```javascript
const parts = ['shoulders', 'knees'];
const lyrics = ['head', ...parts, 'and', 'toes'];

lyrics;
// 输出:
// (5) ["head", "shoulders", "knees", "and", "toes"]
```

## 16. 默认参数

```javascript
const search = (arr, low = 0, high = arr.length - 1) => {
  return high;
};
search([1, 2, 3, 4, 5]);

// 输出: 4
```

## 17. 将十进制转换为二进制或十六进制

在解决问题的同时，我们可以使用一些内置的方法，例如.toPrecision()或.toFixed()来实现许多帮助功能。

```javascript
const num = 10;

num.toString(2);
// 输出: "1010"
num.toString(16);
// 输出: "a"
num.toString(8);
// 输出: "12"
```

## 18. 使用解构简单交换两值

```javascript
let a = 5;
let b = 8;
[a, b] = [b, a];

[a, b];
// 输出
// (2) [8, 5]
```

## 19. 单行回文检查

嗯，这不是一个整体的速记技巧，但它会让你清楚地了解如何使用弦乐。

```javascript
function checkPalindrome(str) {
  return str == str.split('').reverse().join('');
}
checkPalindrome('naman');
// 输出: true
```

## 20. 将 Object 属性转成属性数组

```javascript
// 使用Object.entries(),Object.keys()和Object.values()
const obj = { a: 1, b: 2, c: 3 };

Object.entries(obj);
// 输出
// (3) [Array(2), Array(2), Array(2)]
// 0: (2) ["a", 1]
// 1: (2) ["b", 2]
// 2: (2) ["c", 3]
// length: 3

Object.keys(obj);
// (3) ["a", "b", "c"]

Object.values(obj);
// (3) [1, 2, 3]
```
